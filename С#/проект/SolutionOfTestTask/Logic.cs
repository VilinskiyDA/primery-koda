﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;


namespace SolutionOfTestTask
{
    internal class NodOfDictionary
    {
        public Dictionary<string, int> words;
        public List<string> sortedWords;
        public Dictionary<char, NodOfDictionary> dictionaryOfReferenceToNodes;

        public NodOfDictionary()
        {
            words = new Dictionary<string, int>();
            dictionaryOfReferenceToNodes = new Dictionary<char, NodOfDictionary>();
        }

        public void Add(string word, int numberOfRepetitions, int numberOfChar)
        {
            words[word] = numberOfRepetitions;
            if (numberOfChar == word.Length)
                return;
            char currentChar = word[numberOfChar];
            if (dictionaryOfReferenceToNodes.ContainsKey(currentChar))
                dictionaryOfReferenceToNodes[currentChar].Add(word, numberOfRepetitions, numberOfChar + 1);
            else
            {
                dictionaryOfReferenceToNodes[currentChar] = new NodOfDictionary();
                dictionaryOfReferenceToNodes[currentChar].Add(word, numberOfRepetitions, numberOfChar + 1);
            }
        }

        public void Sort()
        {
            var sortQery = from word in words
                orderby -word.Value, word.Key
                select word.Key;
            sortedWords = new List<string>();
            int i = 0;
            foreach (var s in sortQery)
            {
                i++;
                sortedWords.Add(s);
                if (i == 10)
                    break;
            }
            foreach (var reference in dictionaryOfReferenceToNodes)
            {
                reference.Value.Sort();
            }
        }
    }


    internal class Dictionary
    {
        private NodOfDictionary _dictionaryIinTree=new NodOfDictionary();
        private bool _dictanaryIsSorted = false;

        public void GetOfConsole()
        {
            int n = Convert.ToInt32(Console.ReadLine());
            string currentWord;
            int currentNumber;
            for (int i = 1; i <= n; i++)
            {

                string[] wordAndNumber;
                var readLine = Console.ReadLine();
                wordAndNumber = readLine.Split(" ".ToCharArray());
                currentWord = wordAndNumber[0];
                currentNumber = Convert.ToInt32(wordAndNumber[1]);
                this.Add(currentWord, currentNumber);
            }
        }

        public void Add(string word, int numberOfRepetitions)
        {
            _dictionaryIinTree.Add(word, numberOfRepetitions, 0);
            _dictanaryIsSorted = false;
        }

        public void Sort()
        {
            _dictionaryIinTree.Sort();
            _dictanaryIsSorted = true;
        }

        public List<string> Get(string query)
        {
            List<string> ret = new List<string>();
            NodOfDictionary currentNode = _dictionaryIinTree;
            for (int i = 0; i < query.Length; i++)
            {
                if (currentNode.dictionaryOfReferenceToNodes.ContainsKey(query[i]))
                {
                    currentNode = currentNode.dictionaryOfReferenceToNodes[query[i]];
                }
                else return ret;
            }
            if (_dictanaryIsSorted)
                return currentNode.sortedWords;
            else
            {
                currentNode.Sort();
                return currentNode.sortedWords;
            }
        }
    }

    internal static class GetingQuery
    {
        public static List<string> GetOfConsole()
        {
            List<string> retList = new List<string>();
            try
            {
                int m = Convert.ToInt32(Console.ReadLine());
                for (int i = 1; i <= m; i++)
                {
                    retList.Add(Console.ReadLine());
                }
            }
            catch (Exception)
            {

                throw new Exception("error recognition request");
            }

            return retList;
        }
    }
}