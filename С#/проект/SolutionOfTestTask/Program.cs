﻿using System;
using System.Collections.Generic;

namespace SolutionOfTestTask
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            Dictionary dictionary=new Dictionary();
            dictionary.GetOfConsole();
            var d1 = DateTime.Now;
            var qeryList = GetingQuery.GetOfConsole();
            dictionary.Sort();
            List<List<string>> resultsList=new List<List<string>>();
            List<string> processedQuery=new List<string>(); 
            foreach (var qery in qeryList)
            {
                int n;
                if (processedQuery.Contains(qery))
                {
                    n = processedQuery.FindIndex((s) => s == qery);
                    resultsList.Add(resultsList[n]);
                    processedQuery.Add(qery);
                }
                else
                {
                    resultsList.Add(dictionary.Get(qery));
                    processedQuery.Add(qery);
                }
            }
            foreach (var list in resultsList)
            {
                foreach (var res in list)
                {
                    Console.WriteLine(res);
                }
                Console.WriteLine("");
            }
        }
    }
}


